import { AnkietaService } from './../ankieta.service';
import { Component, OnInit, ContentChildren, QueryList, ViewChildren } from '@angular/core';
import { PytanieComponent } from '../pytanie/pytanie.component';
import { Ankieta } from '../ankieta';
import { Pytanie } from '../pytanie';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.css']
})
export class GeneratorComponent implements OnInit {
@ViewChildren(PytanieComponent) Pytania: QueryList<PytanieComponent>;
name: string;
data: string;
liczbaPytan: number;
pytania: number[];
ankieta: Ankieta;
form: FormGroup;
  constructor(private router: Router, public ankietaService: AnkietaService) {
    // this.name = '';
    // this.data = '';
    this.liczbaPytan = 0;
    this.pytania = [];
  }

  ngOnInit() {
    this.form = new FormGroup(
      {
      name: new FormControl(this.name, [Validators.required]),
      data_zamkniecia: new FormControl(this.data, [Validators.required])
      },
      { updateOn: 'change' }
      );
  }

  save(): void {
    this.ankieta = new Ankieta();
    // this.ankieta.id = 1;
    this.ankieta.nazwaAnkiety = this.name;
    // this.ankieta.czasZakonczenia = this.data;
    const array = this.Pytania.toArray();
    array.forEach(x => {
      const odp = {nrOdpowiedzi: Number, odpowiedzi: String}[x.tresciOdpowiedzi.length] = [];
      const warunki = {nrPytania: Number, nrOdpowiedzi: Number, spojnik: String}[x.warunki.length] = [];
      for ( let i = 0; i < x.tresciOdpowiedzi.length; i++) {
        odp.push({nrOdpowiedzi: i + 1, odpowiedz: x.tresciOdpowiedzi[i]});
      }
      x.Warunki.forEach( k => {

        const war = {nrPytania: Number(k.numerPytania), nrOdpowiedzi: Number(k.numerOpowiedzi), spojnik: k.spojnik};
        warunki.push(war);
      });
      const p = new Pytanie(x.numer, x.tresc, x.kontrolne, x.kontrolka, warunki, odp);
      this.ankieta.pytania.push(p);

      });


    this.ankietaService.postAnkieta(this.ankieta).subscribe( x => {
      this.router.navigate(['']);
    });

  }
  dodaj(): void {
    this.liczbaPytan++;
    this.pytania.push(this.liczbaPytan);

  }
  usun(index: number) {
    this.liczbaPytan--;
    this.recount();
  }
  recount(): void {
    this.pytania = [];
    for (let i = 1; i <= this.liczbaPytan; i++) {
      this.pytania.push(i);
    }
  }
}
