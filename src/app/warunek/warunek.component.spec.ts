import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarunekComponent } from './warunek.component';

describe('WarunekComponent', () => {
  let component: WarunekComponent;
  let fixture: ComponentFixture<WarunekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarunekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarunekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
