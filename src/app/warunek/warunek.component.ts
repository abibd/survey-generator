import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-warunek',
  templateUrl: './warunek.component.html',
  styleUrls: ['./warunek.component.css']
})
export class WarunekComponent implements OnInit {

  @Input() numerWarunku: number;
  numerPytania: number;
  numerOpowiedzi: number;
  spojnik: string;

  constructor() {
    this.numerPytania = 0;
    this.numerOpowiedzi = 0;
    this.spojnik = '';
  }

  ngOnInit() {
  }

}
