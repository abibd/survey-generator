import { TestBed } from '@angular/core/testing';

import { PytaniaKontrolkiService } from './pytania-kontrolki.service';

describe('PytaniaKontrolkiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PytaniaKontrolkiService = TestBed.get(PytaniaKontrolkiService);
    expect(service).toBeTruthy();
  });
});
