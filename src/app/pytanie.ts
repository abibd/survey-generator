export class Pytanie {
  nrPytania: number;
  trescPytania: string;
  pytanieKontrolne: boolean;
  reprezentacjaPytania: string;
  warunkiPytania: {nrPytania: number, nrOdpowiedzi: number, spojnik: string}[];
  odpowiedzi: {nrOdpowiedzi: number, odpowiedz: string}[];

  constructor(
      nrPytania?: number,
      trescPytania?: string,
      pytanieKontrolne?: boolean,
      reprezentacjaPytania?: string,
      warunkiPytania?: {nrPytania: number, nrOdpowiedzi: number, spojnik: string}[],
      odpowiedzi?: {nrOdpowiedzi: number, odpowiedz: string}[]
    ) {
    this.nrPytania = nrPytania;
    this.trescPytania = trescPytania;
    this.pytanieKontrolne = pytanieKontrolne;
    this.reprezentacjaPytania = reprezentacjaPytania;
    this.warunkiPytania = warunkiPytania || [];
    this.odpowiedzi = odpowiedzi || [];
    this.warunkiPytania = warunkiPytania || [];
  }

}
