import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Pytanie } from './pytanie';

@Injectable()
export class PytaniaKontrolkiService {
  constructor() { }

  toFormGroup(pytania: Pytanie[] ) {
    const group: any = {};

    pytania.forEach(pytanie => {
      group[pytanie.nrPytania] = pytanie.pytanieKontrolne === true ? new FormControl('', Validators.required)
                                              : new FormControl('');
    });
    return new FormGroup(group);
  }
}
