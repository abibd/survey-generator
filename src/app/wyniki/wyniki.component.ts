import { AnkietaService } from './../ankieta.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-wyniki',
  templateUrl: './wyniki.component.html',
  styleUrls: ['./wyniki.component.css']
})
export class WynikiComponent implements OnInit {
  @Input() wyniki: any[];
  id;

  constructor(
    private route: ActivatedRoute,
    private ankietaService: AnkietaService
    ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getWyniki();
  }

  getWyniki(): void {
    this.ankietaService.getWyniki(this.id).subscribe(w => {
      this.wyniki = w.data.odpowiedzis;
    });
  }

}
