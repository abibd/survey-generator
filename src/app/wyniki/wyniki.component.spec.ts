import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WynikiComponent } from './wyniki.component';

describe('WynikiComponent', () => {
  let component: WynikiComponent;
  let fixture: ComponentFixture<WynikiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WynikiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WynikiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
