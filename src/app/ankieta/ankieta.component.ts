import { ActivatedRoute, Router } from '@angular/router';
import { PytaniaKontrolkiService } from './../pytania-kontrolki.service';
import { AnkietaService } from './../ankieta.service';
import { Component, OnInit, Input } from '@angular/core';
import { Ankieta } from './../ankieta';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ankieta',
  templateUrl: './ankieta.component.html',
  styleUrls: ['./ankieta.component.css'],
  providers: [ PytaniaKontrolkiService ]
})
export class AnkietaComponent implements OnInit {
  @Input() ankieta: Ankieta;
  @Input() pytaniaKontrolne = false;
  @Input() formValue: JSON;
  form: FormGroup;
  payLoad = '';
  id;
  pseudonim = '';

  constructor(
    private ankietaService: AnkietaService,
    private pytaniaKontrolkiService: PytaniaKontrolkiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getAnkieta();

    this.ankietaService.checker.subscribe(a => {
      if (a === true) {
        this.showAnkieta();
      }
    });
  }

  getAnkieta(): void {
    this.ankietaService.getAnkieta(this.id).subscribe(ankieta => {
      // this.ankieta = ankieta;
      this.ankieta = ankieta.data.ankieta;
      this.ankietaService.checker.next(true);
    });
  }

  showAnkieta(): void {
    this.form = this.pytaniaKontrolkiService.toFormGroup(this.ankieta.pytania);
  }

  onSubmit() {
    const that = this;
    if (this.pytaniaKontrolne === true) {
// tslint:disable-next-line: only-arrow-functions
      Object.keys(this.form.value).forEach(function(key) {
        that.formValue[key] = that.form.value[key];
      });

      const payLoadJson = this.formValue;

      this.ankieta.pytania.forEach(pytanie => {
        if (pytanie.reprezentacjaPytania === 'checkbox') {
          payLoadJson[pytanie.nrPytania] = '';
          const checkboxy: NodeListOf<HTMLInputElement> = document.querySelectorAll('[id^="checkbox' + pytanie.nrPytania + '"]');
          checkboxy.forEach(checkbox => {
            payLoadJson[pytanie.nrPytania] = payLoadJson[pytanie.nrPytania] + checkbox.checked + ';';
          });
          payLoadJson[pytanie.nrPytania] = payLoadJson[pytanie.nrPytania].substring(0, payLoadJson[pytanie.nrPytania].length - 1);
        }
      });

      // this.payLoad = JSON.stringify(payLoadJson);

      const odpowiedzi: string[] = [];
// tslint:disable-next-line: only-arrow-functions
      Object.keys(payLoadJson).forEach(function(key) {
        const odpowiedz = {
          nrOdpowiedzi: key,
          odpowiedz: payLoadJson[key].toString()
        };
        let odpowiedzString = (JSON.stringify(odpowiedz)).replace(/\"([^(\")"]+)\":/g, '$1:');
        odpowiedzString = odpowiedzString.replace('"', '');
        odpowiedzString = odpowiedzString.replace('"', '');
        odpowiedzi.push(odpowiedzString);
      });

      this.ankietaService.postAnkietaWyniki(this.id, this.pseudonim, odpowiedzi).subscribe( x => {
        this.router.navigate(['']);
      });

    } else {
      this.formValue = this.form.value;
      let licznikPytanKontrolnych = 0;

      this.ankieta.pytania.forEach(pytanie => {
        if (pytanie.pytanieKontrolne === true) {
          licznikPytanKontrolnych++;
        }
      });

      for (let i = 1; i <= licznikPytanKontrolnych; i++) {
        if (this.form.controls[i].value !== null) {
          this.form.controls[i].disable();
        }
      }

      this.pytaniaKontrolne = true;


      let licznikPytan = 0;
      licznikPytan = licznikPytanKontrolnych;
      let licznikLUB = 0;
      let licznikLUBNiespelnionych = 0;

      this.ankieta.pytania.forEach(pytanie => {
        if (pytanie.pytanieKontrolne !== true) {
          licznikPytan++;
          if (pytanie.warunkiPytania !== []) {
            pytanie.warunkiPytania.forEach(warunek => {

              pytanie.warunkiPytania.forEach(warunek1 => {
                if (warunek1.spojnik === '||') {
                  licznikLUB++;
                }
              });

              if (that.ankieta.pytania.find(p => p.nrPytania === warunek.nrPytania && p.pytanieKontrolne === true)) {
// tslint:disable-next-line: radix
                if (parseInt(that.formValue[warunek.nrPytania]) !== warunek.nrOdpowiedzi) {
                  if (warunek.spojnik === '&&' || warunek.spojnik === '') {
                      that.form.controls[licznikPytan].disable();
                  } else if (warunek.spojnik === '||') {
                    licznikLUBNiespelnionych++;
                  }
                }
              }

              if (licznikLUB === licznikLUBNiespelnionych && licznikLUB !== 0) {
                that.form.controls[licznikPytan].disable();
              }

            });
          }
        }
      });


    }
  }

}
