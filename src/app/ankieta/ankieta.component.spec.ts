import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnkietaComponent } from './ankieta.component';

describe('AnkietaComponent', () => {
  let component: AnkietaComponent;
  let fixture: ComponentFixture<AnkietaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnkietaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnkietaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
