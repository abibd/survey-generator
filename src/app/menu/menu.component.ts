import { AnkietaService } from './../ankieta.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @Input() listaAnkiet: string[];

  constructor(private ankietaService: AnkietaService) { }

  ngOnInit() {
    this.getAnkiety();
  }

  getAnkiety(): void {
    this.ankietaService.getAnkiety().subscribe(listaAnkiet => {
      this.listaAnkiet = listaAnkiet.data.pobierzListeAnkiet;
    });

  }


}
