import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AnkietaComponent } from './ankieta/ankieta.component';
import { KontrolkaComponent } from './kontrolka/kontrolka.component';
import { GeneratorComponent } from './generator/generator.component';
import { PytanieComponent } from './pytanie/pytanie.component';
import { MenuComponent } from './menu/menu.component';
import { WarunekComponent } from './warunek/warunek.component';
import { WynikiComponent } from './wyniki/wyniki.component';

@NgModule({
  declarations: [
    AppComponent,
    AnkietaComponent,
    KontrolkaComponent,
    GeneratorComponent,
    PytanieComponent,
    GeneratorComponent,
    MenuComponent,
    WarunekComponent,
    WynikiComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
