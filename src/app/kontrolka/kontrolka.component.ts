import { Pytanie } from './../pytanie';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-kontrolka',
  templateUrl: './kontrolka.component.html',
  styleUrls: ['./kontrolka.component.css']
})
export class KontrolkaComponent implements OnInit {
  @Input() pytanie: Pytanie;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.pytanie.nrPytania].valid; }

  constructor() { }

  ngOnInit() {
  }

}
