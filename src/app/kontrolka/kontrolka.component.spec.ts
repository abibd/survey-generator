import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KontrolkaComponent } from './kontrolka.component';

describe('KontrolkaComponent', () => {
  let component: KontrolkaComponent;
  let fixture: ComponentFixture<KontrolkaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KontrolkaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KontrolkaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
