import { WynikiComponent } from './wyniki/wyniki.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnkietaComponent } from './ankieta/ankieta.component';
import { GeneratorComponent } from './generator/generator.component';
import { MenuComponent } from './menu/menu.component';
const routes: Routes = [
{path: '', component: MenuComponent},
{path: 'generator', component: GeneratorComponent},
{path: 'ankieta/:id', component: AnkietaComponent },
{path: 'wyniki/:id', component: WynikiComponent }
];
// {path: 'ankieta', component: AnkietaComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
