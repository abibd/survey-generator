import { Component, OnInit, Input, ViewChildren, QueryList } from '@angular/core';
import { Pytanie } from '../pytanie';
import { WarunekComponent } from '../warunek/warunek.component';

@Component({
  selector: 'app-pytanie',
  templateUrl: './pytanie.component.html',
  styleUrls: ['./pytanie.component.css']
})
export class PytanieComponent implements OnInit {
@Input() numer: number;
tresc: string;
kontrolka: string;
iloscOdpowiedzi: number;
iloscWarunkow: number;
odpowiedzi: number[];
warunki: number[];
tresciOdpowiedzi: string[];
pytanie: Pytanie;
kontrolne: boolean;
@ViewChildren(WarunekComponent) Warunki: QueryList<WarunekComponent>;
  constructor() {

    this.tresc = '';
    this.kontrolka = '';
    this.iloscOdpowiedzi = 0;
    this.iloscWarunkow = 0;
    this.odpowiedzi = [];
    this.warunki = [];
    this.tresciOdpowiedzi = [];
    this.kontrolne = false;

  }

  ngOnInit() {
  }
  dodaj_odpowiedz(): void {
    this.iloscOdpowiedzi++;
    this.odpowiedzi.push(this.iloscOdpowiedzi);
    this.tresciOdpowiedzi.push('');

  }
  usun_odpowiedz(index: number) {
    this.iloscOdpowiedzi--;
    this.tresciOdpowiedzi.splice(index, 1);
    this.recount_pytania();
  }
  recount_pytania(): void {
    this.odpowiedzi = [];
    for (let i = 1; i <= this.iloscOdpowiedzi; i++) {
      this.odpowiedzi.push(i);
    }
  }
  dodaj_warunek(): void {
    this.iloscWarunkow++;
    this.warunki.push(this.iloscWarunkow);
  }
  usun_warunek(index: number) {
    this.iloscWarunkow--;
    this.recount_warunki();
  }
  recount_warunki(): void {
    this.warunki = [];
    for (let i = 1; i <= this.iloscWarunkow; i++) {
      this.warunki.push(i);
    }
  }
}
