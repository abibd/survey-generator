import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ankieta } from './ankieta';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnkietaService {
  public checker = new BehaviorSubject<boolean>(true);

  constructor(private http: HttpClient) { }

  private ankietyUrl = 'api/ankiety';
  private serverUrl = 'http://server421383.nazwa.pl:4000/';

  private headers = new HttpHeaders({
    'Content-Type': 'application/graphql'
  });

  // getAnkieta(id: number): Observable<Ankieta> {
  //   this.checker.next(false);
  //   const url = `${this.ankietyUrl}/${id}`;
  //   return this.http.get<Ankieta>(url).pipe(
  //     catchError(this.handleError<Ankieta>(`getAnkieta id=${id}`))
  //   );
  // }

  getAnkieta(id: string): Observable<any> {
    const query = `{
      ankieta(where:{id:"${id}"})
      {
        id
        nazwaAnkiety
        tworca
        {
          login
        }
        czasZakonczenia
        pytania
        {
          id
          nrPytania
          trescPytania
          pytanieKontrolne
          reprezentacjaPytania
          warunkiPytania
                {
            id
            nrPytania
            nrOdpowiedzi
            spojnik
          }
          odpowiedzi
          {
            id
            nrOdpowiedzi
            odpowiedz
          }
        }
      }
    }`;

    this.checker.next(false);
    return this.http.post<any>(this.serverUrl, query, {headers: this.headers}).pipe(
      catchError(this.handleError<any>())
    );
  }

  getAnkiety(): Observable<any> {
    const query = `{
      pobierzListeAnkiet
      {
       id
       nazwaAnkiety
      }
    }`;

    return this.http.post<any>(this.serverUrl, query, {headers: this.headers}).pipe(
      catchError(this.handleError<any>())
    );
  }

  postAnkietaWyniki(id: string, pseudonim: string, odpowiedzi: string[]) {
    const query = `mutation{
      przeslijOdpowiedzi(data:{
        idAnkiety: "${id}"
        pseudonim: "${pseudonim}"
        odpowiedzi: [
          ${odpowiedzi}
        ]
      })
      {
        id
      }
    }`;

    return this.http.post<any>(this.serverUrl, query, {headers: this.headers}).pipe(
      catchError(this.handleError<any>())
    );
  }

  postAnkieta(ankieta: Ankieta) {
    const json = JSON.stringify(ankieta);

    const val = json.replace(/\"([^(\")"]+)\":/g, '$1:');
    // val = val.replace('"', '');
    // val = val.replace('"', '');

    const query = `mutation {
      dodajAnkiete(
        data:
         ${val}
      ){
        id
      }
    }`;
    // data.append('data', json);
    return this.http.post<any>(this.serverUrl, query, {headers: this.headers}).pipe(
      catchError(this.handleError<any>())
    );

  }

  getWyniki(id: string): Observable<any> {
    const query = `{
      odpowiedzis(where:{idAnkiety: {id: "${id}"}}){
        pseudonim
        odpowiedzi{
          nrOdpowiedzi
          odpowiedz
        }
      }
    } `;

    return this.http.post<any>(this.serverUrl, query, {headers: this.headers}).pipe(
      catchError(this.handleError<any>())
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}
